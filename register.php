<?php require('includes/config.php');

//if logged in redirect to members page
if( $user->is_logged_in() ){ header('Location: memberpage.php'); }

//if form has been submitted process it
if(isset($_POST['submit'])){

	//very basic validation
	if(strlen($_POST['username']) < 3){
		$error[] = 'Username is too short.';
	} else {
		$stmt = $db->prepare('SELECT username FROM venue.user WHERE username = :username');
		$stmt->execute(array(':username' => $_POST['username']));
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		if(!empty($row['username'])){
			$error[] = 'Username provided is already in use.';
		}

	}

	if(strlen($_POST['password']) < 3){
		$error[] = 'Password is too short.';
	}

	if(strlen($_POST['passwordConfirm']) < 3){
		$error[] = 'Confirm password is too short.';
	}

	if($_POST['password'] != $_POST['passwordConfirm']){
		$error[] = 'Passwords do not match.';
	}



	//if no errors have been created carry on
	if(!isset($error)){

		//hash the password
		$hashedpassword = $user->password_hash($_POST['password'], PASSWORD_BCRYPT);


		try {

			//insert into database with a prepared statement
			$stmt = $db->prepare('INSERT INTO venue.user (username,password,email,fullname,phone,type) VALUES (:username, :password, :email, :fullname, :phone, :type)');
			$stmt->execute(array(
				':username' => $_POST['username'],
				':password' => $hashedpassword,
                ':email'    => $_POST['email'],
                ':fullname'    => $_POST['fullname'],
                ':phone'    => $_POST['phone'],
                ':type'    => $_POST['type'],

			));
            $id = $db->lastInsertId('id');


			//redirect to index page
			header('Location: register.php?action=joined');
			exit;

		//else catch the exception and show the error.
		} catch(PDOException $e) {
		    $error[] = $e->getMessage();
		}

	}

}
//include header template
require('layout/header.php');
?>

    <div id="login-page" class="row">
        <div class="col s12 z-depth-4 card-panel">
            <form class="login-form" method="post" action="">
                <div class="row">
                    <div class="input-field col s12 center">
                        <img src="images/login-logo.png" alt="" class="circle responsive-img valign profile-image-login">
                        <p class="center login-form-text">Register an account to access UCTS Venue Reservation System</p>
                    </div>
                </div>


    <?php
    //check for any errors
    if(isset($error)){
        foreach($error as $error){
            echo '<p class="bg-danger">'.$error.'</p>';
        }
    }

    //if action is joined show sucess
    if(isset($_GET['action']) && $_GET['action'] == 'joined'){
        echo "<h2 class='bg-success'>Registration successful.</h2>";
    }

    ?>


    <div class="row margin">
        <div class="input-field col s12">
            <i class="mdi-social-person-outline prefix"></i>
            <input id="username" name="username" type="text" value="<?php if(isset($error)){ echo $_POST['username']; } ?>" tabindex="1">
            <label for="username" class="center-align">Username</label>
        </div>
    </div>
    <div class="row margin">
        <div class="input-field col s12">
            <i class="mdi-action-lock-outline prefix"></i>
            <input id="password" name="password" type="password" tabindex="3">
            <label for="password">Password</label>
        </div>
    </div>
    <div class="row margin">
        <div class="input-field col s12">
            <i class="mdi-action-lock-outline prefix"></i>
            <input id="passwordConfirm" name="passwordConfirm" type="password" tabindex="3">
            <label for="passwordConfirm">Confirm Password</label>
        </div>
    </div>
    <div class="row margin">
        <div class="input-field col s12">
            <i class="mdi-communication-email prefix"></i>
            <input id="email" name="email" type="email" value="<?php if(isset($error)){ echo $_POST['email']; } ?>" tabindex="1">
            <label for="email">Email</label>
        </div>
    </div>
    <div class="row margin">
        <div class="input-field col s12">
            <i class="mdi-social-person-add prefix"></i>
            <input id="fullname" name="fullname" type="text" value="<?php if(isset($error)){ echo $_POST['fullname']; } ?>" tabindex="1">
            <label for="fullname">Full Name</label>
        </div>
    </div>
    <div class="row margin">
        <div class="input-field col s12">
            <i class="mdi-communication-phone prefix"></i>
            <input id="phone" name="phone" type="tel" value="<?php if(isset($error)){ echo $_POST['phone']; } ?>" tabindex="1">
            <label for="phone">Phone</label>
        </div>
    </div>
    <div class="row margin">
        <div class="input-field col s12">
            <i class="mdi-editor-merge-type prefix"></i>
            <input id="type" name="type" type="text" value="<?php if(isset($error)){ echo $_POST['type']; } ?>" tabindex="1">
            <label for="type">Account Type</label>
        </div>
    </div>

    <div class="row">
        <div class="input-field col s12">
            <input type="submit" name="submit" value="Register" class="btn waves-effect waves-light col s12" tabindex="5">
        </div>
    </div>
    <div class="input-field col s12">
        <p class="margin center medium-small sign-up">Already have an account? <a href="index.php">Login</a></p>
    </div>
            </form>
        </div>
    </div>
<?php
//include header template
require('layout/footer.php');
?>