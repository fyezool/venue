<?php require('includes/config.php');

//if not logged in redirect to login page
if(!$user->is_logged_in()){ header('Location: login.php'); }
//fetching logged in data
$result = $db->query("SELECT * FROM user");
$usershow = $result->fetch(PDO::FETCH_ASSOC);

//fetching room data
$block1 = $db->query("SELECT `Room_ID`, `Room_Num`,`Room_Floor` FROM `room` WHERE `Room_Block` = 'Block 1';");


$block2 = $db->query("SELECT `Room_ID`, `Room_Num`,`Room_Floor` FROM `room` WHERE `Room_Block` = 'Block 2';");


$block3 = $db->query("SELECT `Room_ID`, `Room_Num`,`Room_Floor` FROM `room` WHERE `Room_Block` = 'Block 3';");


$block4 = $db->query("SELECT `Room_ID`, `Room_Num`,`Room_Floor` FROM `room` WHERE `Room_Block` = 'Block 4';");


$block5 = $db->query("SELECT `Room_ID`, `Room_Num`,`Room_Floor` FROM `room` WHERE `Room_Block` = 'Block 5';");


$block6 = $db->query("SELECT `Room_ID`, `Room_Num`,`Room_Floor` FROM `room` WHERE `Room_Block` = 'Block 6';");


$block7 = $db->query("SELECT `Room_ID`, `Room_Num`,`Room_Floor` FROM `room` WHERE `Room_Block` = 'Block 7';");


$block8 = $db->query("SELECT `Room_ID`, `Room_Num`,`Room_Floor` FROM `room` WHERE `Room_Block` = 'Block 8';");


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="msapplication-tap-highlight" content="no">
    <title>UCTS Venue Reservation</title>

    <!-- Favicons-->
    <link rel="icon" href="images/favicon/favicon-32x32.png" sizes="32x32">
    <!-- Favicons-->
    <link rel="apple-touch-icon-precomposed" href="images/favicon/apple-touch-icon-152x152.png">
    <!-- For iPhone -->
    <meta name="msapplication-TileColor" content="#00bcd4">
    <meta name="msapplication-TileImage" content="images/favicon/mstile-144x144.png">
    <!-- For Windows Phone -->


    <!-- CORE CSS-->
    <link href="css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="css/style.min.css" type="text/css" rel="stylesheet" media="screen,projection">
    <!-- Custome CSS-->
    <link href="css/custom/custom.min.css" type="text/css" rel="stylesheet" media="screen,projection">

    <!-- INCLUDED PLUGIN CSS ON THIS PAGE -->
    <link href="js/plugins/prism/prism.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="js/plugins/perfect-scrollbar/perfect-scrollbar.css" type="text/css" rel="stylesheet" media="screen,projection">
    <link href="js/plugins/chartist-js/chartist.min.css" type="text/css" rel="stylesheet" media="screen,projection">
</head>

<body>
<!-- Start Page Loading -->
<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>
<!-- End Page Loading -->

<!-- //////////////////////////////////////////////////////////////////////////// -->

<!-- START HEADER -->
<header id="header" class="page-topbar">
    <!-- start header nav-->
    <div class="navbar-fixed">
        <nav class="navbar-color">
            <div class="nav-wrapper">
                <ul class="left">
                    <li><h1 class="logo-wrapper"><a href="index.html" class="brand-logo darken-1"><img src="images/materialize-logo.png" alt="materialize logo"></a> <span class="logo-text">Materialize</span></h1></li>
                </ul>
                <ul class="right hide-on-med-and-down">
                    <li><a href="javascript:void(0);" class="waves-effect waves-block waves-light toggle-fullscreen"><i class="mdi-action-settings-overscan"></i></a>
                    </li>
                    <li><a href="#" data-activates="chat-out" class="waves-effect waves-block waves-light chat-collapse"><i class="mdi-communication-chat"></i></a>
                    </li>
                </ul>

        </nav>
    </div>
    <!-- end header nav-->
</header>
<!-- END HEADER -->

<!-- //////////////////////////////////////////////////////////////////////////// -->

<!-- START MAIN -->
<div id="main">
    <!-- START WRAPPER -->
    <div class="wrapper">

        <!-- START LEFT SIDEBAR NAV-->
        <aside id="left-sidebar-nav">
            <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details cyan darken-2">
                    <div class="row">
                        <div class="col col s4 m4 l4">
                            <img src="images/avatar.png" alt="" class="circle responsive-img valign profile-image">
                        </div>
                        <div class="col col s4 m4 l4">

                        </div>
                        <div class="col col s8 m8 l8">
                            <ul id="profile-dropdown" class="dropdown-content">
                                <li><a href="#"><i class="mdi-action-face-unlock"></i> Profile</a>
                                </li>
                                <li><a href="#"><i class="mdi-action-settings"></i> Settings</a>
                                </li>
                                <li><a href="#"><i class="mdi-communication-live-help"></i> Help</a>
                                </li>
                                <li class="divider"></li>
                                <li><a href="#"><i class="mdi-action-lock-outline"></i> Lock</a>
                                </li>
                                <li><a href="logout.php"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                                </li>
                            </ul>
                            <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" data-activates="profile-dropdown"><?php echo $usershow['username']; ?><i class="mdi-navigation-arrow-drop-down right"></i></a>

                            <p class="user-roal"><?php echo $usershow['type']; ?></p>
                        </div>
                    </div>
                </li>
                <li class="bold"><a href="memberpage.php" class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i>Home</a>
                </li>
                <li class="bold"><a href="available_room.php" class="waves-effect waves-cyan"><i class="mdi-communication-email"></i> Available room </a>
                </li>
                <li class="bold"><a href="all_booking.php" class="waves-effect waves-cyan"><i class="mdi-editor-insert-invitation"></i> View  All booking</a>
                </li>
                <li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                        <li class="bold"><a class="collapsible-header waves-effect waves-cyan"><i class="mdi-av-queue"></i> Booking </a>
                            <div class="collapsible-body">
                                <ul>

                                    <li><a href="book_room.php">Book a room</a>
                                    </li>
                                    <li><a href="advanced-ui-modals.html">View all facilities</a>
                                    </li>

                            </div>
                        </li>

                        <li class="li-hover"><div class="divider"></div></li>
                        <li class="li-hover"><p class="ultra-small margin more-text">MORE</p></li>
                        <li><a href="angular-ui.html"><i class="mdi-action-verified-user"></i> About us </a>
                        </li>
                        <li><a href="css-grid.html"><i class="mdi-image-grid-on"></i> Contact Us</a>
                        </li>
                        <li><a href="css-color.html"><i class="mdi-editor-format-color-fill"></i> Meet our Team</a>
                        </li>
                        <li><a href="css-helpers.html"><i class="mdi-communication-live-help"></i> Map</a>
                        </li>



                    </ul>
                    <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only cyan"><i class="mdi-navigation-menu"></i></a>
        </aside>
        <!-- END LEFT SIDEBAR NAV-->

        <!-- //////////////////////////////////////////////////////////////////////////// -->

        <!-- START CONTENT -->
        <section id="content">

            <!--breadcrumbs start-->
            <div id="breadcrumbs-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <h5 class="breadcrumbs-title">Available room</h5>

                        </div>
                    </div>
                </div>
            </div>
            <!--breadcrumbs end-->


            <!--start container-->
            <div class="container">
                <div class="section">

                    <p class="caption">All Available Room for booking. </p>
                    <div class="container">
                        <div class="section">
                            <div class="divider"></div>
                            <div class="row">

                                <section class="plans-container" id="plans">
                                    <article class="col s12 m6 l4">
                                        <div class="card hoverable">
                                            <div class="card-image purple waves-effect">
                                                <div class="card-title">Block 1</div>
                                                <div class="price">


                                                </div>
                                            </div>
                                            <div class="card-content">
                                                <ul class="collection">
                                                    <table width='80%' border=0 align="center">

                                                        <tr bgcolor='#CCCCCC' align="center">
                                                            <td>Room ID</td>
                                                            <td>Room Number</td>
                                                            <td>Room Floor</td>




                                                        </tr>
                                                        <?php
                                                        while ($block1available1 = $block1->fetch(PDO::FETCH_ASSOC)) {
                                                            echo "<tr align='center'>";
                                                            echo "<td>" . $block1available1['Room_ID'] . "</td>";
                                                            echo "<td>" . $block1available1['Room_Num'] . "</td>";
                                                            echo "<td>" . $block1available1['Room_Floor'] . "</td>";

                                                        }
                                                        ?>
                                                    </table>
                                                </ul>
                                            </div>
                                            <div class="card-action center-align">
                                                <button class="waves-effect waves-light  btn"><a href="book_room.php">Book Room</a> </button>
                                            </div>
                                        </div>
                                    </article>
                                    <article class="col s12 m6 l4">
                                        <div class="card z-depth-1 hoverable">
                                            <div class="card-image cyan waves-effect">
                                                <div class="card-title">Block 2</div>
                                            </div>
                                            <div class="card-content">
                                                <ul class="collection">

                                                    <table width='80%' border=0 align="center">

                                                        <tr bgcolor='#CCCCCC' align="center">
                                                            <td>Room ID</td>
                                                            <td>Room Number</td>
                                                            <td>Room Floor</td>




                                                        </tr>
                                                        <?php
                                                        while ($block1available2 = $block2->fetch(PDO::FETCH_ASSOC)) {
                                                            echo "<tr align='center'>";
                                                            echo "<td>" . $block1available2['Room_ID'] . "</td>";
                                                            echo "<td>" . $block1available2['Room_Num'] . "</td>";
                                                            echo "<td>" . $block1available2['Room_Floor'] . "</td>";

                                                        }
                                                        ?>
                                                    </table>

                                                </ul>
                                            </div>
                                            <div class="card-action center-align">
                                                <button class="waves-effect waves-light  btn"><a href="book_room.php">Book Room</a> </button>
                                            </div>
                                        </div>
                                    </article>
                                    <article class="col s12 m6 l4">
                                        <div class="card hoverable">
                                            <div class="card-image green waves-effect">
                                                <div class="card-title">Block 3</div>

                                            </div>
                                            <div class="card-content">
                                                <ul class="collection">
                                                    <table width='80%' border=0 align="center">

                                                        <tr bgcolor='#CCCCCC' align="center">
                                                            <td>Room ID</td>
                                                            <td>Room Number</td>
                                                            <td>Room Floor</td>




                                                        </tr>
                                                        <?php
                                                        while ($block1available3 = $block3->fetch(PDO::FETCH_ASSOC)) {
                                                            echo "<tr align='center'>";
                                                            echo "<td>" . $block1available3['Room_ID'] . "</td>";
                                                            echo "<td>" . $block1available3['Room_Num'] . "</td>";
                                                            echo "<td>" . $block1available3['Room_Floor'] . "</td>";


                                                        }
                                                        ?>
                                                    </table>
                                                </ul>
                                            </div>
                                            <div class="card-action center-align">
                                                <button class="waves-effect waves-light  btn"><a href="book_room.php">Book Room</a> </button>
                                            </div>
                                        </div>
                                    </article>
                                    <article class="col s12 m6 l4">
                                        <div class="card hoverable">
                                            <div class="card-image green waves-effect">
                                                <div class="card-title">Block 4</div>

                                            </div>
                                            <div class="card-content">
                                                <ul class="collection">
                                                    <table width='80%' border=0 align="center">

                                                        <tr bgcolor='#CCCCCC' align="center">
                                                            <td>Room ID</td>
                                                            <td>Room Number</td>
                                                            <td>Room Floor</td>




                                                        </tr>
                                                        <?php
                                                        while ($block1available4 = $block4->fetch(PDO::FETCH_ASSOC)) {
                                                            echo "<tr align='center'>";
                                                            echo "<td>" . $block1available4['Room_ID'] . "</td>";
                                                            echo "<td>" . $block1available4['Room_Num'] . "</td>";
                                                            echo "<td>" . $block1available4['Room_Floor'] . "</td>";


                                                        }
                                                        ?>
                                                    </table>
                                                </ul>
                                            </div>
                                            <div class="card-action center-align">
                                                <button class="waves-effect waves-light  btn"><a href="book_room.php">Book Room</a> </button>
                                            </div>
                                        </div>
                                    </article>
                                    <article class="col s12 m6 l4">
                                        <div class="card hoverable">
                                            <div class="card-image green waves-effect">
                                                <div class="card-title">Block 5</div>

                                            </div>
                                            <div class="card-content">
                                                <ul class="collection">
                                                    <table width='80%' border=0 align="center">

                                                        <tr bgcolor='#CCCCCC' align="center">
                                                            <td>Room ID</td>
                                                            <td>Room Number</td>
                                                            <td>Room Floor</td>




                                                        </tr>
                                                        <?php
                                                        while ($block1available5 = $block5->fetch(PDO::FETCH_ASSOC)) {
                                                            echo "<tr align='center'>";
                                                            echo "<td>" . $block1available5['Room_ID'] . "</td>";
                                                            echo "<td>" . $block1available5['Room_Num'] . "</td>";
                                                            echo "<td>" . $block1available5['Room_Floor'] . "</td>";


                                                        }
                                                        ?>
                                                    </table>
                                                </ul>
                                            </div>
                                            <div class="card-action center-align">
                                                <button class="waves-effect waves-light  btn"><a href="book_room.php">Book Room</a> </button>
                                            </div>
                                        </div>
                                    </article>
                                    <article class="col s12 m6 l4">
                                        <div class="card hoverable">
                                            <div class="card-image green waves-effect">
                                                <div class="card-title">Block 6</div>

                                            </div>
                                            <div class="card-content">
                                                <ul class="collection">
                                                    <table width='80%' border=0 align="center">

                                                        <tr bgcolor='#CCCCCC' align="center">
                                                            <td>Room ID</td>
                                                            <td>Room Number</td>
                                                            <td>Room Floor</td>




                                                        </tr>
                                                        <?php
                                                        while ($block1available6 = $block6->fetch(PDO::FETCH_ASSOC)) {
                                                            echo "<tr align='center'>";
                                                            echo "<td>" . $block1available6['Room_ID'] . "</td>";
                                                            echo "<td>" . $block1available6['Room_Num'] . "</td>";
                                                            echo "<td>" . $block1available6['Room_Floor'] . "</td>";

                                                        }
                                                        ?>
                                                    </table>
                                                </ul>
                                            </div>
                                            <div class="card-action center-align">
                                                <button class="waves-effect waves-light  btn"><a href="book_room.php">Book Room</a> </button>
                                            </div>
                                        </div>
                                    </article>
                                    <article class="col s12 m6 l4">
                                        <div class="card hoverable">
                                            <div class="card-image green waves-effect">
                                                <div class="card-title">Block 7</div>

                                            </div>
                                            <div class="card-content">
                                                <ul class="collection">
                                                    <table width='80%' border=0 align="center">

                                                        <tr bgcolor='#CCCCCC' align="center">
                                                            <td>Room ID</td>
                                                            <td>Room Number</td>
                                                            <td>Room Floor</td>




                                                        </tr>
                                                        <?php
                                                        while ($block1available7 = $block7->fetch(PDO::FETCH_ASSOC)) {
                                                            echo "<tr align='center'>";
                                                            echo "<td>" . $block1available7['Room_ID'] . "</td>";
                                                            echo "<td>" . $block1available7['Room_Num'] . "</td>";
                                                            echo "<td>" . $block1available7['Room_Floor'] . "</td>";


                                                        }
                                                        ?>
                                                    </table>
                                                </ul>
                                            </div>
                                            <div class="card-action center-align">
                                                <button class="waves-effect waves-light  btn"><a href="book_room.php">Book Room</a> </button>
                                            </div>
                                        </div>
                                    </article>
                                    <article class="col s12 m6 l4">
                                        <div class="card hoverable">
                                            <div class="card-image green waves-effect">
                                                <div class="card-title">Block 8</div>

                                            </div>
                                            <div class="card-content">
                                                <ul class="collection">
                                                    <table width='80%' border=0 align="center">

                                                        <tr bgcolor='#CCCCCC' align="center">
                                                            <td>Room ID</td>
                                                            <td>Room Number</td>
                                                            <td>Room Floor</td>




                                                        </tr>
                                                        <?php
                                                        while ($block1available8 = $block8->fetch(PDO::FETCH_ASSOC)) {
                                                            echo "<tr align='center'>";
                                                            echo "<td>" . $block1available8['Room_ID'] . "</td>";
                                                            echo "<td>" . $block1available8['Room_Num'] . "</td>";
                                                            echo "<td>" . $block1available8['Room_Floor'] . "</td>";


                                                        }
                                                        ?>
                                                    </table>
                                                </ul>
                                            </div>
                                            <div class="card-action center-align">
                                                <button class="waves-effect waves-light  btn"><a href="book_room.php">Book Room</a> </button>
                                            </div>
                                        </div>
                                    </article>
                                </section>
                            </div>
                        </div>
                    </div>






                </div>
                <!-- Floating Action Button -->
                <div class="fixed-action-btn" style="bottom: 50px; right: 19px;">
                    <a class="btn-floating btn-large">
                        <i class="mdi-action-stars"></i>
                    </a>
                    <ul>
                        <li><a href="css-helpers.html" class="btn-floating red"><i class="large mdi-communication-live-help"></i></a></li>
                        <li><a href="app-widget.html" class="btn-floating yellow darken-1"><i class="large mdi-device-now-widgets"></i></a></li>
                        <li><a href="app-calendar.html" class="btn-floating green"><i class="large mdi-editor-insert-invitation"></i></a></li>
                        <li><a href="app-email.html" class="btn-floating blue"><i class="large mdi-communication-email"></i></a></li>
                    </ul>
                </div>
                <!-- Floating Action Button -->
            </div>
            <!--end container-->
        </section>
        <!-- END CONTENT -->

        <!-- //////////////////////////////////////////////////////////////////////////// -->
        <!-- START RIGHT SIDEBAR NAV-->
        <aside id="right-sidebar-nav">
            <ul id="chat-out" class="side-nav rightside-navigation">
                <li class="li-hover">
                    <a href="#" data-activates="chat-out" class="chat-close-collapse right"><i class="mdi-navigation-close"></i></a>
                    <div id="right-search" class="row">
                        <form class="col s12">
                            <div class="input-field">
                                <i class="mdi-action-search prefix"></i>
                                <input id="icon_prefix" type="text" class="validate">
                                <label for="icon_prefix">Search</label>
                            </div>
                        </form>
                    </div>
                </li>
                <li class="li-hover">
                    <ul class="chat-collapsible" data-collapsible="expandable">
                        <li>
                            <div class="collapsible-header teal white-text active"><i class="mdi-social-whatshot"></i>Recent Activity</div>
                            <div class="collapsible-body recent-activity">
                                <div class="recent-activity-list chat-out-list row">
                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-action-add-shopping-cart"></i>
                                    </div>
                                    <div class="col s9 recent-activity-list-text">
                                        <a href="#">just now</a>
                                        <p>Jim Doe Purchased new equipments for zonal office.</p>
                                    </div>
                                </div>
                                <div class="recent-activity-list chat-out-list row">
                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-device-airplanemode-on"></i>
                                    </div>
                                    <div class="col s9 recent-activity-list-text">
                                        <a href="#">Yesterday</a>
                                        <p>Your Next flight for USA will be on 15th August 2015.</p>
                                    </div>
                                </div>
                                <div class="recent-activity-list chat-out-list row">
                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                                    </div>
                                    <div class="col s9 recent-activity-list-text">
                                        <a href="#">5 Days Ago</a>
                                        <p>Natalya Parker Send you a voice mail for next conference.</p>
                                    </div>
                                </div>
                                <div class="recent-activity-list chat-out-list row">
                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-action-store"></i>
                                    </div>
                                    <div class="col s9 recent-activity-list-text">
                                        <a href="#">Last Week</a>
                                        <p>Jessy Jay open a new store at S.G Road.</p>
                                    </div>
                                </div>
                                <div class="recent-activity-list chat-out-list row">
                                    <div class="col s3 recent-activity-list-icon"><i class="mdi-action-settings-voice"></i>
                                    </div>
                                    <div class="col s9 recent-activity-list-text">
                                        <a href="#">5 Days Ago</a>
                                        <p>Natalya Parker Send you a voice mail for next conference.</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="collapsible-header light-blue white-text active"><i class="mdi-editor-attach-money"></i>Sales Repoart</div>
                            <div class="collapsible-body sales-repoart">
                                <div class="sales-repoart-list  chat-out-list row">
                                    <div class="col s8">Target Salse</div>
                                    <div class="col s4"><span id="sales-line-1"></span>
                                    </div>
                                </div>
                                <div class="sales-repoart-list chat-out-list row">
                                    <div class="col s8">Payment Due</div>
                                    <div class="col s4"><span id="sales-bar-1"></span>
                                    </div>
                                </div>
                                <div class="sales-repoart-list chat-out-list row">
                                    <div class="col s8">Total Delivery</div>
                                    <div class="col s4"><span id="sales-line-2"></span>
                                    </div>
                                </div>
                                <div class="sales-repoart-list chat-out-list row">
                                    <div class="col s8">Total Progress</div>
                                    <div class="col s4"><span id="sales-bar-2"></span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="collapsible-header red white-text"><i class="mdi-action-stars"></i>Favorite Associates</div>
                            <div class="collapsible-body favorite-associates">
                                <div class="favorite-associate-list chat-out-list row">              <section class="plans-container" id="plans">
                <article class="col s12 m6 l4">
                  <div class="card hoverable">
                    <div class="card-image purple waves-effect">
                      <div class="card-title">BASIC</div>
                      <div class="price"><sup>$</sup>9<sub>/mo</sub></div>
                      <div class="price-desc">Free 1 month</div>
                    </div>
                    <div class="card-content">
                      <ul class="collection">
                        <li class="collection-item">500 emails</li>
                        <li class="collection-item">unlimited data</li>
                        <li class="collection-item">1 users</li>
                        <li class="collection-item">first 15 day free</li>
                      </ul>
                    </div>
                    <div class="card-action center-align">
                      <button class="waves-effect waves-light  btn">Select Plan</button>
                    </div>
                  </div>
                </article>
                <article class="col s12 m6 l4">
                  <div class="card z-depth-1 hoverable">
                    <div class="card-image cyan waves-effect">
                      <div class="card-title">PROFESSIONAL</div>
                      <div class="price"><sup>$</sup>29<sub>/mo</sub></div>
                      <div class="price-desc">Most Popular</div>
                    </div>
                    <div class="card-content">
                      <ul class="collection">
                        <li class="collection-item">2000 emails</li>
                        <li class="collection-item">unlimited data</li>
                        <li class="collection-item">10 users</li>
                        <li class="collection-item">first 30 day free</li>
                      </ul>
                    </div>
                    <div class="card-action center-align">
                      <button class="waves-effect waves-light  btn">Select Plan</button>
                    </div>
                  </div>
                </article>
                <article class="col s12 m6 l4">
                  <div class="card hoverable">
                    <div class="card-image green waves-effect">
                      <div class="card-title">PREMIUM</div>
                      <div class="price"><sup>$</sup>49<sub>/mo</sub></div>
                      <div class="price-desc">Get 20% off</div>
                    </div>
                    <div class="card-content">
                      <ul class="collection">
                        <li class="collection-item">10,000 emails</li>
                        <li class="collection-item">unlimited data</li>
                        <li class="collection-item">unlimited users</li>
                        <li class="collection-item">first 90 day free</li>
                      </ul>
                    </div>
                    <div class="card-action center-align">
                      <button class="waves-effect waves-light  btn">Select Plan</button>
                    </div>
                  </div>
                </article>
              </section>
                                    <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                                    </div>
                                    <div class="col s8">
                                        <p>Eileen Sideways</p>
                                        <p class="place">Los Angeles, CA</p>
                                    </div>
                                </div>
                                <div class="favorite-associate-list chat-out-list row">
                                    <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                                    </div>
                                    <div class="col s8">
                                        <p>Zaham Sindil</p>
                                        <p class="place">San Francisco, CA</p>
                                    </div>
                                </div>
                                <div class="favorite-associate-list chat-out-list row">
                                    <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                                    </div>
                                    <div class="col s8">
                                        <p>Renov Leongal</p>
                                        <p class="place">Cebu City, Philippines</p>
                                    </div>
                                </div>
                                <div class="favorite-associate-list chat-out-list row">
                                    <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img online-user valign profile-image">
                                    </div>
                                    <div class="col s8">
                                        <p>Weno Carasbong</p>
                                        <p>Tokyo, Japan</p>
                                    </div>
                                </div>
                                <div class="favorite-associate-list chat-out-list row">
                                    <div class="col s4"><img src="images/avatar.jpg" alt="" class="circle responsive-img offline-user valign profile-image">
                                    </div>
                                    <div class="col s8">
                                        <p>Nusja Nawancali</p>
                                        <p class="place">Bangkok, Thailand</p>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </aside>
        <!-- LEFT RIGHT SIDEBAR NAV-->

    </div>
    <!-- END WRAPPER -->

</div>
<!-- END MAIN -->



<!-- //////////////////////////////////////////////////////////////////////////// -->

<!-- START FOOTER -->
<footer class="page-footer">
    <div class="footer-copyright">
        <div class="container">
            <span>Copyright © 2018 <a class="grey-text text-lighten-4" href="http://themeforest.net/user/geekslabs/portfolio?ref=geekslabs" target="_blank">Gorillaslab</a> All rights reserved.</span>
            <span class="right"> Design and Developed by <a class="grey-text text-lighten-4" href="http://geekslabs.com/">Gorillaslab</a></span>
        </div>
    </div>
</footer>
<!-- END FOOTER -->



<!-- ================================================
Scripts
================================================ -->

<!-- jQuery Library -->
<script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>
<!--materialize js-->
<script type="text/javascript" src="js/materialize.min.js"></script>
<!--prism
<script type="text/javascript" src="js/prism/prism.js"></script>-->
<!--scrollbar-->
<script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<!-- chartist -->
<script type="text/javascript" src="js/plugins/chartist-js/chartist.min.js"></script>

<!--plugins.js - Some Specific JS codes for Plugin Settings-->
<script type="text/javascript" src="js/plugins.min.js"></script>
<!--custom-script.js - Add your own theme custom JS-->
<script type="text/javascript" src="js/custom-script.js"></script>

</body>

</html>